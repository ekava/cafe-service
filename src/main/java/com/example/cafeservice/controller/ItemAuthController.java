package com.example.cafeservice.controller;

import com.example.cafeservice.dto.item.ItemRequestDto;
import com.example.cafeservice.dto.item.ItemResponseDto;
import com.example.cafeservice.service.interfaces.ItemService;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth/items")
public class ItemAuthController {

    private final ItemService itemService;

    @RolesAllowed("owner")
    @PostMapping
    public void createItem(@RequestBody ItemRequestDto itemDto) {
        itemService.createItem(itemDto);
    }

    @RolesAllowed({"owner", "worker"})
    @GetMapping
    public List<ItemResponseDto> getAllItems() {
        return itemService.getAllItems();
    }

    @RolesAllowed({"owner", "worker"})
    @GetMapping("/{id}")
    public ItemResponseDto getById(@PathVariable UUID id) {
        return itemService.getItemById(id);
    }
}
