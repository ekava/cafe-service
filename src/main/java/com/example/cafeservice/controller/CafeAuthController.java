package com.example.cafeservice.controller;

import com.example.cafeservice.dto.cafe.CafeRequestDto;
import com.example.cafeservice.dto.cafe.CafeResponseDto;
import com.example.cafeservice.service.interfaces.CafeService;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth/cafes")
public class CafeAuthController {

    private final CafeService cafeService;

    @RolesAllowed("owner")
    @PostMapping
    public void createCafe(@RequestBody CafeRequestDto cafeRequestDto) {
        cafeService.createCafe(cafeRequestDto);
    }

    @RolesAllowed("owner")
    @GetMapping
    public List<CafeResponseDto> getAll() {
        return cafeService.getAllCafes();
    }

    @RolesAllowed({"owner", "worker"})
    @GetMapping("/{id}")
    public CafeResponseDto getCafeById(@PathVariable UUID id) {
        return cafeService.getCafeById(id);
    }

    @GetMapping("/hello")
    @RolesAllowed("user")
    public String hello() {
        return "Hello, World!";
    }
}
