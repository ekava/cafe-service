package com.example.cafeservice.exception.custom;

import org.springframework.http.HttpStatus;

public class CafeNotFoundException extends BadRequestException {

    public CafeNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Cafe has not been found.");
    }
}
