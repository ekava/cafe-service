package com.example.cafeservice.dto.stock;

import java.io.Serializable;
import java.util.UUID;

/**
 * DTO for {@link com.example.cafeservice.entity.Stock}
 */
public record StockRequestDto(
    UUID itemId,
    UUID cafeId,
    UUID transactionId) {
}