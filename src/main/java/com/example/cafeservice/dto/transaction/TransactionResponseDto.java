package com.example.cafeservice.dto.transaction;


import com.example.cafeservice.entity.enums.TransactionStatus;
import com.example.cafeservice.entity.enums.TransactionType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

public record TransactionResponseDto(UUID id,
                                     LocalDateTime date,
                                     UUID clientId,
                                     UUID itemId,
                                     BigDecimal price,
                                     TransactionType type,
                                     TransactionStatus status) implements Serializable {
}