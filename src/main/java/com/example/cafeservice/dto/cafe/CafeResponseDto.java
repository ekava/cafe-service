package com.example.cafeservice.dto.cafe;

import java.io.Serializable;
import java.util.UUID;

/**
 * DTO for {@link com.example.cafeservice.entity.Cafe}
 */
public record CafeResponseDto(UUID id,
                              String name,
                              String city,
                              String address) {
}