package com.example.cafeservice.dto.cafe;

import jakarta.validation.constraints.NotNull;

import java.io.Serializable;

/**
 * DTO for {@link com.example.cafeservice.entity.Cafe}
 */
public record CafeRequestDto(@NotNull String name,
                             @NotNull String city,
                             @NotNull String address) {
}