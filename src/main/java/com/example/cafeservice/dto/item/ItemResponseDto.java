package com.example.cafeservice.dto.item;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * DTO for {@link com.example.cafeservice.entity.Item}
 */
public record ItemResponseDto(UUID id,
                              String name,
                              String description,
                              BigDecimal buyPrice) {
}