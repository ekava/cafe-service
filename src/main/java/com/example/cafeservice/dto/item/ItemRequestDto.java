package com.example.cafeservice.dto.item;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * DTO for {@link com.example.cafeservice.entity.Item}
 */
public record ItemRequestDto(String name,
                             String description,
                             BigDecimal buyPrice) {
}