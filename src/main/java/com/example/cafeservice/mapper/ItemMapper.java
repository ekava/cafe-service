package com.example.cafeservice.mapper;

import com.example.cafeservice.dto.item.ItemRequestDto;
import com.example.cafeservice.dto.item.ItemResponseDto;
import com.example.cafeservice.entity.Item;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ItemMapper {

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Item itemRequestDtoToItem(ItemRequestDto itemRequestDto);

    ItemResponseDto itemToItemResponseDto(Item item);
}
