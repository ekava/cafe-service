package com.example.cafeservice.mapper;

import com.example.cafeservice.dto.transaction.TransactionResponseDto;
import com.example.cafeservice.entity.Stock;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StockMapper {

  @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
  Stock transactionToStock(TransactionResponseDto transactionResponseDto);
}
