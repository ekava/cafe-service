package com.example.cafeservice.mapper;

import com.example.cafeservice.dto.cafe.CafeRequestDto;
import com.example.cafeservice.dto.cafe.CafeResponseDto;
import com.example.cafeservice.entity.Cafe;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CafeMapper {

    @Mapping(target = "id", expression = "java(java.util.UUID.randomUUID())")
    Cafe cafeRequestDtoToCafe(CafeRequestDto cafeDto);

    List<CafeResponseDto> cafesToCafesResponseDto(List<Cafe> cafes);

    CafeResponseDto cafeToCafeResponseDto(Cafe cafe);
}
