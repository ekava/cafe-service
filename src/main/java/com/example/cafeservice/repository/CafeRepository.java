package com.example.cafeservice.repository;

import com.example.cafeservice.entity.Cafe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CafeRepository extends JpaRepository<Cafe, UUID> {
}