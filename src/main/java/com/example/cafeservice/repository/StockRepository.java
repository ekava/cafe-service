package com.example.cafeservice.repository;

import com.example.cafeservice.entity.Stock;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, UUID> {
}