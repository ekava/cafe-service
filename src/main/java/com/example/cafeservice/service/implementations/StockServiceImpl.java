package com.example.cafeservice.service.implementations;

import static com.example.cafeservice.entity.enums.TransactionStatus.ACCEPTED;
import static com.example.cafeservice.entity.enums.TransactionStatus.DECLINED;

import com.example.cafeservice.dto.transaction.TransactionResponseDto;
import com.example.cafeservice.entity.Stock;
import com.example.cafeservice.entity.enums.TransactionStatus;
import com.example.cafeservice.mapper.StockMapper;
import com.example.cafeservice.repository.StockRepository;
import com.example.cafeservice.service.interfaces.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {

  private final StockRepository stockRepository;
  private final StockMapper stockMapper;

  @Override
  public TransactionStatus changeStock(TransactionResponseDto transactionResponseDto) {
    try {
      Stock stock = stockMapper.transactionToStock(transactionResponseDto);
      stockRepository.save(stock);
      return ACCEPTED;
    } catch (Exception e) {
      return DECLINED;
    }
  }
}
