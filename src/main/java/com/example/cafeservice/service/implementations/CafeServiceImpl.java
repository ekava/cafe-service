package com.example.cafeservice.service.implementations;

import com.example.cafeservice.dto.cafe.CafeRequestDto;
import com.example.cafeservice.dto.cafe.CafeResponseDto;
import com.example.cafeservice.entity.Cafe;
import com.example.cafeservice.exception.custom.BadRequestException;
import com.example.cafeservice.exception.custom.CafeNotFoundException;
import com.example.cafeservice.mapper.CafeMapper;
import com.example.cafeservice.repository.CafeRepository;
import com.example.cafeservice.service.interfaces.CafeService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CafeServiceImpl implements CafeService {

    private final CafeRepository cafeRepository;
    private final CafeMapper cafeMapper;

    @Override
    public void createCafe(CafeRequestDto cafeDto) {
        Cafe cafe = cafeMapper.cafeRequestDtoToCafe(cafeDto);
        try {
            cafeRepository.save(cafe);
        } catch (DataAccessException ex) {
            throw new BadRequestException("Unable to save cafe");
        }
    }

    @Override
    public List<CafeResponseDto> getAllCafes() {
        try {
            return cafeMapper.cafesToCafesResponseDto(cafeRepository.findAll());
        } catch (DataAccessException ex) {
            throw new BadRequestException("Unable to get cafes from DB.");
        }
    }

    @Override
    public CafeResponseDto getCafeById(UUID id) {
        return cafeMapper.cafeToCafeResponseDto(cafeRepository.findById(id)
                .orElseThrow(CafeNotFoundException::new));
    }
}
