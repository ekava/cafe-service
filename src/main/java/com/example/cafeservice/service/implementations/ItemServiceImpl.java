package com.example.cafeservice.service.implementations;

import com.example.cafeservice.dto.item.ItemRequestDto;
import com.example.cafeservice.dto.item.ItemResponseDto;
import com.example.cafeservice.exception.custom.BadRequestException;
import com.example.cafeservice.mapper.ItemMapper;
import com.example.cafeservice.repository.ItemRepository;
import com.example.cafeservice.service.interfaces.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class ItemServiceImpl implements ItemService {

    private final ItemRepository itemRepository;
    private final ItemMapper itemMapper;

    @Override
    public void createItem(ItemRequestDto itemDto) {
        try {
            itemRepository.save(itemMapper.itemRequestDtoToItem(itemDto));
        } catch (DataAccessException ex) {
            throw new BadRequestException("Can't save item.");
        }
    }

    @Override
    public List<ItemResponseDto> getAllItems() {
        try {
            return itemRepository.findAll().stream()
                    .map(itemMapper::itemToItemResponseDto)
                    .toList();
        } catch (DataAccessException ex) {
            throw new BadRequestException("Can't get items from repository.");
        }
    }

    @Override
    public ItemResponseDto getItemById(UUID id) {
        return itemRepository.findById(id)
                .map(itemMapper::itemToItemResponseDto)
                .orElseThrow(() -> new BadRequestException("Can't get Item from repository."));
    }
}
