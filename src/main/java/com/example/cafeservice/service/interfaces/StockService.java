package com.example.cafeservice.service.interfaces;

import com.example.cafeservice.dto.transaction.TransactionResponseDto;
import com.example.cafeservice.entity.enums.TransactionStatus;

public interface StockService {
  TransactionStatus changeStock(TransactionResponseDto transactionResponseDto);
}
