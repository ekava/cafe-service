package com.example.cafeservice.service.interfaces;

import com.example.cafeservice.dto.item.ItemRequestDto;
import com.example.cafeservice.dto.item.ItemResponseDto;

import java.util.List;
import java.util.UUID;

public interface ItemService {

    /**
     * TODO
     * @param itemDto
     */
    void createItem(ItemRequestDto itemDto);

    /**
     * TODO
     * @return
     */
    List<ItemResponseDto> getAllItems();

    /**
     * TODO
     * @param id
     * @return
     */
    ItemResponseDto getItemById(UUID id);
}
