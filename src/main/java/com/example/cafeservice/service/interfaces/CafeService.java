package com.example.cafeservice.service.interfaces;

import com.example.cafeservice.dto.cafe.CafeRequestDto;
import com.example.cafeservice.dto.cafe.CafeResponseDto;

import java.util.List;
import java.util.UUID;

public interface CafeService {

    /**
     * TODO
     * @param cafeDto
     */
    void createCafe(CafeRequestDto cafeDto);

    /**
     * TODO
     * @return
     */
    List<CafeResponseDto> getAllCafes();

    /**
     * TODO
     * @param id
     * @return
     */
    CafeResponseDto getCafeById(UUID id);
}
