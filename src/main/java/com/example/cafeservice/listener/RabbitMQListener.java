package com.example.cafeservice.listener;

import static com.example.cafeservice.entity.enums.TransactionStatus.DECLINED;

import com.example.cafeservice.dto.transaction.TransactionResponseDto;
import com.example.cafeservice.entity.enums.TransactionStatus;
import com.example.cafeservice.service.interfaces.StockService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@EnableRabbit
@Component
@RequiredArgsConstructor
@Slf4j
public class RabbitMQListener {

    private final ObjectMapper objectMapper;
    private final StockService stockService;

    @RabbitListener(queues = "transaction-queue")
    public String processTransaction(String message) {
        log.info(message);
        try {
            var transactionResponseDto = objectMapper.readValue(message, TransactionResponseDto.class); // TODO add processing of transaction
            return stockService.changeStock(transactionResponseDto).toString();
        } catch (JsonProcessingException ex) {
            log.error("Unable to get message through rabbitmq.");
            return DECLINED.toString();
        }
    }
}
