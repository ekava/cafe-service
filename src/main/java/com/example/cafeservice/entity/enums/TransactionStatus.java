package com.example.cafeservice.entity.enums;

public enum TransactionStatus {
    ACCEPTED,
    DECLINED,
    PROCESSING
}
