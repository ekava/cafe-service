package com.example.cafeservice.entity.enums;

public enum TransactionType {
    BUY,
    SELL,
    FIX
}
