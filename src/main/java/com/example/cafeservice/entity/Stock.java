package com.example.cafeservice.entity;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Stock {
  @Id
  @Column(nullable = false, updatable = false)
  @JdbcTypeCode(SqlTypes.UUID)
  private UUID id;

  private UUID itemId;

  private UUID cafeId;

  private UUID transactionId;
}
